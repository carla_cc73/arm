## Default

$templateFile="arm_template.json"
$rg="RgIsCarlaTrainingUSSc"
$paramFileDefault="arm_template_parameters_default.json"
$paramFileCareer="arm_template_parameters_only_career.json"
$paramFileStudent="arm_template_parameters_only_student.json"

az deployment group create `
  --name df-arm-parameters `
  --resource-group $rg `
  --template-file $templateFile `
  --parameters $paramFileDefault

templateFile="arm_template.json"
rg="RgIsCarlaTrainingUSSc"
paramFileDefault="arm_template_parameters_default.json"
paramFileCareer="arm_template_parameters_only_career.json"
paramFileStudent="arm_template_parameters_only_student.json"

az deployment group create \
  --name df-arm-parameters \
  --resource-group $rg \
  --template-file $templateFile \
  --parameters $paramFileDefault

## Only Career

az deployment group create `
  --name df-arm-parameters `
  --resource-group $rg `
  --template-file $templateFile `
  --parameters $paramFileCareer

az deployment group create \
  --name df-arm-parameters \
  --resource-group $rg \
  --template-file $templateFile \
  --parameters $paramFileCareer

## Only Student

az deployment group create `
  --name df-arm-parameters `
  --resource-group $rg `
  --template-file $templateFile `
  --parameters $paramFileStudent

az deployment group create \
  --name df-arm-parameters \
  --resource-group $rg \
  --template-file $templateFile \
  --parameters $paramFileStudent