$templateFile="factory/DfTrainingArmFunctions_arm_template.json"
$parameterFile="factory/DfTrainingArmFunctions_arm_template_parameters.json"
$rg="RgIsCarlaTrainingUSSc"

az deployment group create `
  --name df-arm-functions `
  --resource-group $rg `
  --template-file $templateFile `
  --parameters $parameterFile

templateFile="arm_template.json"
parameterFile="arm_template_parameters.json"
rg="RgIsCarlaTrainingUSSc"

az deployment group create \
  --name df-arm-functions \
  --resource-group $rg \
  --template-file $templateFile \
  --parameters $parameterFile