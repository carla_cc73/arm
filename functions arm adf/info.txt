ADF control flow expression language functions

You can call this functions whitin expressions. The following sections provide information about
the functions that can be used in an expression

ARM template functions
Functions you can use in an Azure Resource Manager (ARM) template
Most functions work the same when deployed to a resource group, subscription, management group, or tenant. 
A few functions can't be used in all scopes.