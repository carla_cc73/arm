USE [training]
GO

/****** Object:  Table [dbo].[StudentInfoCSV]    Script Date: 10/8/2020 5:04:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StudentInfoCSV](
	[StudentID] [int] NOT NULL,
	[StudentName] [varchar](50) NOT NULL,
	[CareerName] [varchar](50) NOT NULL,
	[StudentCareerLevel] [tinyint] NOT NULL,
	[Timestamp] [datetime] NOT NULL
) ON [PRIMARY]
GO


